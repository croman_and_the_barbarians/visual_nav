# Visual_Nav

visual_nav uses frequency based image processing methods _(Reddy & Chatterji, 1996) (Cassagrande, 2011)_ to stitch images
together and implement stereo vision to convert pixel translation into real world translation in mm.

visual_nav spicifically does this via ROS framework, via processing .bag files for mosaic-able images and header times, as well as nortek_dvl data
and other sensors to compare data to other sensor results.

In addition, 'class_node_driver_node_visual_nav.cpp' will serve as a sensor driver that will subscribe to ros topics for real time velocity data collection.

##**Dependencies**
ros-noetic-desktop
opencv4
Eigen3


##**General Instructions**
Create a directory ~/catkin_ws/src
Add the following two lines to ~/.bashrc: source '/opt/ros/noetic/setup.bash source ~/catkin_ws/devel/setup.bash'
Clone pf_nav into ~/catkin_ws/src
cd ~/catkin_ws
catkin_make

##**Customization for your project
Configure input and output topics, and which sensor sources to use, in class_node_bag_reader_visual_nav.cpp. For an example, see lines 433-490
Configure launch file in visual_nav/launch/export.launch, or use the existing for post processing a rosbag or relaying active topics
The output variables are automatically saved via CSV format for use in matlab, or python for data visualization

Maintained by: Jordan Beason, Roman Lab, University of Rhode Island's Graduate School of Oceanography.

