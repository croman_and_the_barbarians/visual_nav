#include <ros/ros.h>
#include <rosbag/view.h>
#include "visual_nav.h"
#include <std_msgs/String.h>
#include <cv_bridge/cv_bridge.h>
#include "opencv4/opencv2/opencv.hpp"
#include <image_transport/image_transport.h>
#include <iostream>
#include <vector>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

using namespace cv;
using namespace std;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "bagwriter");
  TemplateClass my_class;
  // %Tag(WRITE)%
  if(argc < 3){
    ROS_WARN("please enter a valid path & filename, followed by a valid path to source the images from");
    return 1;
  }


  cv::String path(argv[2]);
  vector<cv::String> fn;
  vector<cv::Mat> data;
  cv::glob(path,fn,true); // recurse


  rosbag::Bag bag;
  bag.open(argv[1], rosbag::bagmode::Write);
  for (size_t k=0; k<fn.size(); ++k)
  {


       cv::Mat im = cv::imread(fn[k]);
       if (im.empty()) continue;
       std::cout << "loading image " << k << "\n";
       data.push_back(im);

       cv_bridge::CvImage img_bridge;
       sensor_msgs::Image data_msg;

       std_msgs::Header header;
       //header.seq = counter;

       img_bridge = cv_bridge::CvImage(header, sensor_msgs::image_encodings::BGR8, data[k]);
       img_bridge.toImageMsg(data_msg);
       bag.write("/chatter",ros::Time::now(),data_msg);

  }
  bag.close();
  // %EndTag(WRITE)%
}
