#include "visual_nav.h"

using namespace cv;
using namespace cv::xfeatures2d;
using namespace Eigen;
using namespace std;

//YAML FILE PATH FOR CALIBRATION DATA
static string xml_path = "/home/jbeason/catkin_ws/src/visual_nav/visual_nav/data/tank_calibration.xml";

//Global variables
static int cols = 640; //1504.0
static int rows = 480; //2056.0 (Settings for no artifacts, but is slower and less accurate)
static int Cx = rows/2;
static int Cy = cols/2;
static double result_scale = .5;
static unsigned long impoststart = 2 ; //Start image for post process //356 good disp example //36
static unsigned long impostend = 200; // End image for post process
static int wait = 1; // Wait period for end of script // INPUT 0 FOR SPACEBAR USE
static unsigned long array_limit = 20  ; //# of Images saved for mosaic reconstruction

static cv::Mat A = (Mat_<double>(3,3) << 7.0828659057617188e+02, 0., 3.2180118179321289e+02, 0.,
               7.0828659057617188e+02, 2.3295171546936035e+02, 0., 0., 1. );

struct StereoCalData {
  cv::Mat size;
  cv::Mat Q;
  cv::Mat Rot;
  cv::Mat Trans;
  cv::Mat Ess;
  cv::Mat Fund;
};

cv::Mat splitL(Mat input_image)
{
    // Define the ROI for the left side of the image
    Rect roi(0, 0, 640, 480);

    // Extract the left side of the image
    Mat output_image = input_image(roi);
    Mat result;
    output_image.copyTo(result);

    return result;
}

// Function to split a 1280x480 image into two 640x480 images, returning the right side
cv::Mat splitR(Mat input_image)
{
    // Define the ROI for the right side of the image
    Rect roi(640, 0, 640, 480);

    // Extract the right side of the image
    Mat output_image = input_image(roi);
    Mat result;
    output_image.copyTo(result);

    return result;
}

StereoCalData readPiCamData(const std::string& filename) {
  StereoCalData data;
  cv::FileStorage xml(filename, cv::FileStorage::READ);

  if (!xml.isOpened()) {
      std::cerr << "Failed to open file" << std::endl;
  }

  xml["Image_Size"] >> data.size;
  xml["Q_matrix"] >> data.Q;
  xml["Rotation_matrix"] >> data.Rot;
  xml["Translation_matrix"] >> data.Trans;
  xml["Essential_matrix"] >> data.Ess;
  xml["Fundamental_matrix"] >> data.Fund;


  xml.release();
  // Read cv::Mat from YAML file
  //fs["matrix"] >> mat;

  return data;
}

int main(int argc, char** argv)
{

  ros::init(argc, argv, "bagreader_processor");  //ROS stuff
  ros::NodeHandle nh;

  std::ofstream myfile;
  myfile.open("CSV_10.csv");
  myfile << "Image Time, Image X, Image Y, Image Altitude, Phase X, Phase Y, Response, DVL Altitude Time, DVL Altitude, DVL Twist Time, DVL X, DVL Y, NAVSAT Twist Time, NAVSAT X, NAVSAT Y, ODOMETRY \n";

  //YAML Loading
  StereoCalData picam =readPiCamData(xml_path);
  cv::Mat image, imageR,R1,R2,P1,P2,Q, map11,map12,map21,map22,hist, Disparity;
  Rect validRoi[2];


  //cv::stereoRectify(floatcam.A,floatcam.DistA,floatcam.B,floatcam.DistB,cv::Size(4112,3008), floatcam.Rot, floatcam.Trans, R1,R2,P1,P2,Q,0,-1,cv::Size(4112,3008),&validRoi[0],&validRoi[1]);
  //std::cout << "Q Matrix Check: " << Q << std::endl;
  //cv::initUndistortRectifyMap(floatcam.A,floatcam.DistA,R1,P1,cv::Size(4112,3008), CV_32FC1,map11,map12);
  //cv::initUndistortRectifyMap(floatcam.B,floatcam.DistB,R2,P2,cv::Size(4112,3008), CV_32FC1,map21,map22);
  cv::Mat HPkernel = generateCosineSquaredHighpassFilter(640,480,1.0,1.0); //width,height,cutoff freq, order
  //std::cout << "Kernel Check: " << HPkernel << std::endl;
  //generateCosineSquaredFilter()

  unsigned long int clear_counter = 0;
  unsigned long int clearR_counter = 0;
  vector<cv::Mat> input, inputR; //make vector of images
  vector<unsigned long int> image_number_track; //make vector to track r images
  ros::Time img_timestamp, timestampimage,dvl_altitude_timestamp, navsat_timestamp, odometry_timestamp;

  unsigned long int vector_size; //take size
  unsigned long int vector_counter;

  double dvl_altitude,odometry_x,odometry_y;
  geometry_msgs::Vector3::_x_type navsat_x;
  geometry_msgs::Vector3::_y_type navsat_y;
  ros::Time dvl_twist_timestamp;
  geometry_msgs::Vector3::_x_type dvl_x;
  geometry_msgs::Vector3::_y_type dvl_y;

  if(argc < 2){                                         //the program uses argv[1] as the path to the bag, already set up in QTCreator
    ROS_WARN("please enter a valid path & filename");
    return 1;
  }

  rosbag::Bag bag;                                    //Initialize and open the bag file
  std::string image_topic_name = "/chatter"; // /stereo/left/image_raw
  std::string image_right_topic_name = "/stereo/right/image_raw";
  std::string camera_info_topic_name = "/stereo/left/camera_info";
  std::string navsat_pose_topic_name = "/nav/sensors/navsat/ubx_hdg/relpos_enu"; // /nav/processed/nortek/dvl_twist";
  std::string dvl_twist_topic_name = "/nav/processed/nortek/dvl_twist";
  std::string dvl_altitude_topic_name = "/nav/sensors/nortek/ranges";
  //std::cout << "searching bag in topic name: " << image_topic_name << std::endl;
  bag.open(argv[1]);

  for (rosbag::MessageInstance const m : rosbag::View(bag)) {  //Process the images in batch
    {
      sensor_msgs::CameraInfo::ConstPtr i = m.instantiate<sensor_msgs::CameraInfo>();
      std::string imgTopiccaminfo = m.getTopic();
      if (i != nullptr) {
        sensor_msgs::CameraInfo info = *i;
        img_timestamp = info.header.stamp;
        //camera_info == imgTopiccaminfo
        //std::cout << "Cam Info" << i->K << std::endl;
        //sensor_msgs::CameraInfo
        //static boost::array<double, 9 > intrinsic = i->K;
        //std::cout << "Time Test: " << img_timestamp<< std::endl;
      }
    }
    {
      ds_sensor_msgs::DvlConstPtr i = m.instantiate<ds_sensor_msgs::Dvl>();
      std::string DVLaltitudetopic = m.getTopic();
      if (i != nullptr) {
        ds_sensor_msgs::Dvl dvldata = *i;
        dvl_altitude_timestamp = dvldata.header.stamp;
        dvl_altitude = dvldata.altitude;
      }
    }
    {
      nav_msgs::OdometryConstPtr i = m.instantiate<nav_msgs::Odometry>();
      if (i != nullptr) {
        nav_msgs::Odometry Odometry = *i;
        odometry_timestamp = Odometry.header.stamp;
        odometry_x = Odometry.twist.twist.linear.x;
        odometry_y = Odometry.twist.twist.linear.y;
      }
    }
    {
      geometry_msgs::PoseWithCovarianceStampedPtr i = m.instantiate<geometry_msgs::PoseWithCovarianceStamped>();
      std::string NAVPoseTopic = m.getTopic();
      if (navsat_pose_topic_name == NAVPoseTopic){
        geometry_msgs::PoseWithCovarianceStamped Pose = *i;
        navsat_timestamp = Pose.header.stamp;
        navsat_x = Pose.pose.pose.position.x;
        navsat_y = Pose.pose.pose.position.y;
        //std::cout << "Time check: " << twist.twist.twist.linear.y << std::endl;
      }
    }
    {
      geometry_msgs::TwistWithCovarianceStampedPtr i = m.instantiate<geometry_msgs::TwistWithCovarianceStamped>();
      std::string DVLTwistTopic = m.getTopic();
      if (dvl_twist_topic_name == DVLTwistTopic){
        geometry_msgs::TwistWithCovarianceStamped twist = *i;
        dvl_twist_timestamp = twist.header.stamp;
        dvl_x = twist.twist.twist.linear.x;
        dvl_y = twist.twist.twist.linear.y;
        //std::cout << "Twist X: " << twist.twist.twist.linear.x << std::endl;
      }
    }
    {
       sensor_msgs::ImageConstPtr i = m.instantiate<sensor_msgs::Image>();
       std::string imgTopicright = m.getTopic();
       if (image_right_topic_name == imgTopicright) {
         imageR = cv_bridge::toCvCopy(i)->image;
         sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "CV_8UC1", imageR).toImageMsg();
         //std::cout << "Image R type: " << imageR.type() << std::endl;
         //cv::imshow("imageR", Resize(imageR));
         //imageR = applyHistogram(imageR);
         //imageR.convertTo(imageR,CV_32FC1,1/255.0);
         //cv::remap(imageR,imageR, map21, map22, cv::INTER_LINEAR);
         inputR.push_back(imageR);
      }
    }
    {
      std::string imgTopicleft = m.getTopic();
      if (image_topic_name == imgTopicleft) {
         sensor_msgs::ImageConstPtr imgMsgPtr = m.instantiate<sensor_msgs::Image>();
         timestampimage = imgMsgPtr->header.stamp;
         image = cv_bridge::toCvCopy(imgMsgPtr)->image;
         sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "CV_8UC1", image).toImageMsg();
         input.push_back(image); //add image to vector








         vector_size = input.size()+(clear_counter*10);                 // Count image arrays to manage memory and navigate rosbag
         vector_counter = input.size();
         image_number_track.push_back(vector_size);                    // Save to check for sync
         std::cout << "Loading Image: " << vector_size << std::endl;


         if(vector_counter >= array_limit){                              // Memory management Left camera
           input.erase(input.begin(),input.begin()+10);
           clear_counter += 1;
           std::cout << "Clearing Data Left... " << std::endl;
           vector_counter -= 10;
         }


         if(vector_size >= impoststart){  // Image processing, done in the loop as images are loaded in
           cv::Mat current_dft, last_dft, current_dft_LP, last_dft_LP, current_dft_LP2, last_dft_LP2, current_fix_win, current_win,last_win,current_dft_hpf,last_dft_hpf,window,window2;

           cv::Mat current = splitL(input[vector_counter-1]); // Input without CLAHE
           cv::Mat last = splitL(input[vector_counter-2]);

           cv::cvtColor(current, current, cv::COLOR_BGR2GRAY);
           cv::cvtColor(last, last, cv::COLOR_BGR2GRAY);

           current.convertTo(current,CV_32FC1,1.0/255.0); // CV32 required for Phase Correlation Functions
           last.convertTo(last,CV_32FC1,1.0/255.0);
           cv::imshow("Test", current);
           cv::waitKey(2000);

           current = HighPass(current, HPkernel); // Input & CLAHE
           last = HighPass(last, HPkernel);

           current = applyHistogram(current); // Input & CLAHE
           last = applyHistogram(last);

           current_win = Window(current);
           last_win = Window(last);

           current_dft = FFT(current_win);
           last_dft = FFT(last_win);

           logpolar(current_dft,current_dft_LP2);  //EXPERIMENTAL, MIT LICENSE
           logpolar(last_dft,last_dft_LP2);

           createHanningWindow(window, current_dft_LP2.size(), CV_32F); //current_dft2_LP
           Point2d phaseLP = cv::phaseCorrelate(current_dft_LP2,last_dft_LP2,window);//current_dft2_LP, last_dft2_LP

           Point2d scale_rot = Scale_Rot(phaseLP, current.size()); //return rotate, scale values
           cv::Mat current_fix = Scale_Rot_Fix(current,scale_rot); //return current_fix
           cv::Mat currentcolor_fix = Scale_Rot_Fix(currentcolor,scale_rot);

           createHanningWindow(window2, current.size(), CV_32F);
           double response;
           current_fix_win = Window(current_fix);
           Point2d phase_fix=cv::phaseCorrelate(last_win,current_win,noArray(),&response); //current_dft_fix,last_dft

           Phase_Stitch(phase_fix, current_fix, last);//current_fix , current

           Matrix<double,3,1> Translation = Translate(phase_fix,dvl_altitude,A); // or use disparity altitude.dvl_altitude
           double Translation_x = Translation[0];
           double Translation_y = Translation[1];
           double Translation_z = Translation[2];//return x,y

           myfile << timestampimage << ", "
                  << Translation_x << ", "
                  << Translation_y << ", "
                  << Translation_z << ", "
                  << phase_fix.x << ", "
                  << phase_fix.y << ", "
                  << response << ", "
                  << dvl_altitude_timestamp << ", "
                  << dvl_altitude << ", "
                  << dvl_twist_timestamp << ", "
                  << dvl_x << ", "
                  << dvl_y << ", "
                  << navsat_timestamp << ", "
                  << navsat_x << ", "
                  << navsat_y << ", "
                  << odometry_timestamp << ", "
                  << odometry_x << ", "
                  << odometry_y << "\n";
           //myfile.close();
           std::cout << "SAVED CSV #############################################" << std::endl;
        //   SURFshow(current,last);
           cv::waitKey(wait);

           if (vector_size >= impostend){

             bag.close();
             std::cout << "bag closed...program end" << std::endl;


             return 0;
           }
    }
   }
  }
  }
}
