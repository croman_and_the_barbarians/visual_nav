#include "visual_nav.h"

using namespace cv;
using namespace cv::xfeatures2d;
using namespace Eigen;
using namespace std;

//YAML FILE PATH FOR CALIBRATION DATA
static string yaml_path = "/home/jbeason/catkin_ws/src/visual_nav/visual_nav/data/stereo2m.yml";

//Global variables
static int cols = 4112; //1504.0
static int rows = 3008; //2056.0 (Settings for no artifacts, but is slower and less accurate)
static int Cx = rows/2;
static int Cy = cols/2;
static double result_scale = .5;
static unsigned long impoststart = 2; //Start image for post process //356 good disp example //36
static unsigned long impostend = 1637; // End image for post process
static int wait = 1; // Wait period for end of script // INPUT 0 FOR SPACEBAR USE
static unsigned long array_limit = 20  ; //# of Images saved for mosaic reconstruction

/*
static cv::Mat A = (Mat_<double>(3,3) << 2.6988829333112853e+03, 0., 2.0583815031249073e+03, 0.,
                2.6991978365382488e+03, 1.4559202790089334e+03, 0., 0., 1. );
static cv::Mat B = (Mat_<double>(3,3) << 2.6997352557440677e+03, 0., 2.0699054152632134e+03, 0.,
                2.7018705112903908e+03, 1.4543589856208089e+03, 0., 0., 1.);
static cv::Mat DistA = (Mat_<double>(1,5) << -5.6867760758207014e-02, -1.5928488071668363e-02,
                    -3.1337783460169450e-03, 6.2746376724926616e-04,
                    7.1677156381785942e-02 );
static cv::Mat DistB = (Mat_<double>(1,5) << -6.2049898377041945e-02, 2.2948183973167818e-02,
                    -2.7782282423326047e-03, 6.2034889392291199e-04,
                    3.2824979152582708e-02);
static cv::Mat Rot = (Mat_<double>(3,3) << 9.9999296123265247e-01, 1.0978239102489792e-03,
                  -3.5877942824196992e-03, -1.1056818713831272e-03,
                  9.9999699299203570e-01, -2.1889436917831287e-03,
                  3.5853804191706838e-03, 2.1928952434140782e-03,
                  9.9999116808984934e-01 );
static cv::Mat Trans = (Mat_<double>(3,1) << -1.0018946932036482e+02, 1.0874025799625169e-01,
                    -4.5442660693579029e+00 );
static cv::Mat Fund = (Mat_<double>(3,3) << -1.3641340564060824e-09, 1.3374447549781734e-06,
                 -1.8659270564745380e-03, -1.2308216645108260e-06,
                 6.3141092265349587e-08, 8.1978880506054161e-02,
                 1.7944997714571221e-03, -8.2463882620971082e-02,
                 9.9999999999999989e-01 );
*/

struct StereoCalData {
  double reprojectionError;
  cv::Mat A;
  cv::Mat B;
  cv::Mat DistA;
  cv::Mat DistB;
  cv::Mat Rot;
  cv::Mat Trans;
  cv::Mat Ess;
  cv::Mat Fund;
};

StereoCalData readStereoCalData(const std::string& filename) {
  StereoCalData data;
  cv::FileStorage yaml(filename, cv::FileStorage::READ);

  // Read cv::Mat from YAML file
  //fs["matrix"] >> mat;
  if (!yaml.isOpened()) {
         std::cerr << "Failed to open file." << std::endl;
     }

  yaml["reprojectionError"] >> data.reprojectionError;
  yaml["cameraMatrixA"] >> data.A;
  yaml["cameraMatrixB"] >> data.B;
  yaml["distortionCoeffA"] >> data.DistA;
  yaml["distortionCoeffB"] >> data.DistB;
  yaml["rotation"] >> data.Rot;
  yaml["translation"] >> data.Trans;
  yaml["essential"] >> data.Ess;
  yaml["fundamental"] >> data.Fund;

  // Close YAML file
  yaml.release();

  //return data;
  return data;
}

int main(int argc, char** argv)
{

  ros::init(argc, argv, "bagreader_processor");  //ROS stuff
  ros::NodeHandle nh;

  std::ofstream myfile;
  myfile.open("CSV_YAK_02.csv");
  myfile << "Image Time, Image X, Image Y, Image Altitude, Phase X, Phase Y, Response, DVL Altitude Time, DVL Altitude, DVL Twist Time, DVL X, DVL Y, NAVSAT Twist Time, NAVSAT X, NAVSAT Y, ODOMETRY \n";

  //YAML Loading
  StereoCalData floatcam =readStereoCalData(yaml_path);
  cv::Mat image, imageR,R1,R2,P1,P2,Q, map11,map12,map21,map22,hist, Disparity;
  Rect validRoi[2];


  cv::stereoRectify(floatcam.A,floatcam.DistA,floatcam.B,floatcam.DistB,cv::Size(4112,3008), floatcam.Rot, floatcam.Trans, R1,R2,P1,P2,Q,0,-1,cv::Size(4112,3008),&validRoi[0],&validRoi[1]);
  //std::cout << "Q Matrix Check: " << Q << std::endl;
  cv::initUndistortRectifyMap(floatcam.A,floatcam.DistA,R1,P1,cv::Size(4112,3008), CV_32FC1,map11,map12);
  cv::initUndistortRectifyMap(floatcam.B,floatcam.DistB,R2,P2,cv::Size(4112,3008), CV_32FC1,map21,map22);
  cv::Mat HPkernel = generateCosineSquaredHighpassFilter(1028,752,1.0,1.0); //width,height,cutoff freq, order
  //std::cout << "Kernel Check: " << HPkernel << std::endl;
  //generateCosineSquaredFilter()

  unsigned long int clear_counter = 0;
  unsigned long int clearR_counter = 0;
  vector<cv::Mat> input, inputR; //make vector of images
  vector<unsigned long int> image_number_track; //make vector to track r images
  ros::Time img_timestamp, timestampimage,dvl_altitude_timestamp, navsat_timestamp, odometry_timestamp;

  vector<double> Altitude;
  double Altitude_out;
  unsigned long int vector_size; //take size
  unsigned long int vectorR_size;
  unsigned long int Altitude_size = Altitude.size();
  unsigned long int vector_counter;
  unsigned long int vectorR_counter;


  double dvl_altitude,odometry_x,odometry_y;
  geometry_msgs::Vector3::_x_type navsat_x;
  geometry_msgs::Vector3::_y_type navsat_y;
  ros::Time dvl_twist_timestamp;
  geometry_msgs::Vector3::_x_type dvl_x;
  geometry_msgs::Vector3::_y_type dvl_y;



  TemplateClass my_class;
  image_transport::ImageTransport it(nh);          //This section is for publishing the images to a topic
  std::string image_pub_topic_name = "/chatter";
  image_transport::Publisher pub = it.advertise(image_pub_topic_name, 1);

  if(argc < 2){                                         //the program uses argv[1] as the path to the bag, already set up in QTCreator
    ROS_WARN("please enter a valid path & filename");
    return 1;
  }

  rosbag::Bag bag;                                    //Initialize and open the bag file
  std::string image_topic_name = "/stereo/left/image_raw"; // /stereo/left/image_raw
  std::string image_right_topic_name = "/stereo/right/image_raw";
  std::string navsat_pose_topic_name = "/nav/sensors/navsat/ubx_hdg/relpos_enu"; // /nav/processed/nortek/dvl_twist";
  std::string dvl_twist_topic_name = "/nav/processed/nortek/dvl_twist";
  std::string altitude_topic_name = "/yak_range";
  //std::cout << "searching bag in topic name: " << image_topic_name << std::endl;
  bag.open(argv[1]);

  for (rosbag::MessageInstance const m : rosbag::View(bag)) {  //Process the images in batch
    {
      sensor_msgs::CameraInfo::ConstPtr i = m.instantiate<sensor_msgs::CameraInfo>();
      std::string imgTopiccaminfo = m.getTopic();
      if (i != nullptr) {
        sensor_msgs::CameraInfo info = *i;
        img_timestamp = info.header.stamp;
        //camera_info == imgTopiccaminfo
        //std::cout << "Cam Info" << i->K << std::endl;
        //sensor_msgs::CameraInfo
        //static boost::array<double, 9 > intrinsic = i->K;
        //std::cout << "Time Test: " << img_timestamp<< std::endl;
      }
    }
    {
      sensor_msgs::RangePtr i = m.instantiate<sensor_msgs::Range>();
      std::string RANGESaltitudetopic = m.getTopic();
      if (altitude_topic_name == RANGESaltitudetopic) {
        //ds_sensor_msgs::Dvl dvldata = *i;
        sensor_msgs::Range dvldata = *i;
        dvl_altitude_timestamp = dvldata.header.stamp;
        dvl_altitude = dvldata.range;
      }
    }
    {
      nav_msgs::OdometryConstPtr i = m.instantiate<nav_msgs::Odometry>();
      if (i != nullptr) {
        nav_msgs::Odometry Odometry = *i;
        odometry_timestamp = Odometry.header.stamp;
        odometry_x = Odometry.twist.twist.linear.x;
        odometry_y = Odometry.twist.twist.linear.y;
      }
    }
    {
      geometry_msgs::PoseWithCovarianceStampedPtr i = m.instantiate<geometry_msgs::PoseWithCovarianceStamped>();
      std::string NAVPoseTopic = m.getTopic();
      if (navsat_pose_topic_name == NAVPoseTopic){
        geometry_msgs::PoseWithCovarianceStamped Pose = *i;
        navsat_timestamp = Pose.header.stamp;
        navsat_x = Pose.pose.pose.position.x;
        navsat_y = Pose.pose.pose.position.y;
        //std::cout << "Time check: " << twist.twist.twist.linear.y << std::endl;
      }
    }
    {
      geometry_msgs::TwistWithCovarianceStampedPtr i = m.instantiate<geometry_msgs::TwistWithCovarianceStamped>();
      std::string DVLTwistTopic = m.getTopic();
      if (dvl_twist_topic_name == DVLTwistTopic){
        geometry_msgs::TwistWithCovarianceStamped twist = *i;
        dvl_twist_timestamp = twist.header.stamp;
        dvl_x = twist.twist.twist.linear.x;
        dvl_y = twist.twist.twist.linear.y;
        //std::cout << "Twist X: " << twist.twist.twist.linear.x << std::endl;
      }
    }
    {
       sensor_msgs::ImageConstPtr i = m.instantiate<sensor_msgs::Image>();
       std::string imgTopicright = m.getTopic();
       if (image_right_topic_name == imgTopicright) {
         imageR = cv_bridge::toCvCopy(i)->image;
         sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "CV_8UC1", imageR).toImageMsg();
         //std::cout << "Image R type: " << imageR.type() << std::endl;
         //cv::imshow("imageR", Resize(imageR));
         //imageR = applyHistogram(imageR);
         //imageR.convertTo(imageR,CV_32FC1,1/255.0);
         //cv::remap(imageR,imageR, map21, map22, cv::INTER_LINEAR);
         inputR.push_back(imageR);
      }
    }
    {
      std::string imgTopicleft = m.getTopic();
      if (image_topic_name == imgTopicleft) {
         sensor_msgs::ImageConstPtr imgMsgPtr = m.instantiate<sensor_msgs::Image>();
         timestampimage = imgMsgPtr->header.stamp;
         image = cv_bridge::toCvCopy(imgMsgPtr)->image;
         sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "CV_8UC1", image).toImageMsg();
         //pub.publish(msg);                 //publish in the specified topic
         //std::cout << "Publishing image to "<< image_pub_topic_name << std::endl;

         //cv::Mat image_log = image;
         //std::cout << "Image L type: " << image.type() << std::endl;
         //cv::cvtColor(image,image)
         //image = applyHistogram(image);
         //image.convertTo(image,CV_32FC1,1/255.0);
         //cv::remap(image,image, map11, map12, cv::INTER_LINEAR);

              //Cos_Filt(image);

              //cv::imshow("image", Resize(image));
              //cv::waitKey(0);
              //cv::medianBlur(image,image,2);
              //cv::GaussianBlur(image, image, cv::Size(9,9), 0, 0, BORDER_DEFAULT);

         input.push_back(image); //add image to vector

         vector_size = input.size()+(clear_counter*10);                 // Count image arrays to manage memory and navigate rosbag
         vectorR_size = inputR.size()+(clearR_counter*10);
         vector_counter = input.size();
         vectorR_counter = inputR.size();
         image_number_track.push_back(vectorR_size);                    // Save to check for sync
         std::cout << "Loading Images [Left, Right]: " << vector_size << ", " << vectorR_size << std::endl;


         if(vector_counter >= array_limit){                              // Memory management Left camera
           input.erase(input.begin(),input.begin()+10);
           clear_counter += 1;
           std::cout << "Clearing Data Left... " << std::endl;
           vector_counter -= 10;
         }

         if(vectorR_counter >= array_limit){                            // Memory management Right camera
           inputR.erase(inputR.begin(),inputR.begin()+10);
           clearR_counter += 1;
           std::cout << "Clearing Data Right... " << std::endl;
           vectorR_counter -= 10;

         }

         if(vector_size >= impoststart){  // Image processing, done in the loop as images are loaded in
           cv::Mat current_dft, last_dft, current_dft_LP, last_dft_LP, current_dft_LP2, last_dft_LP2, current_fix_win, current_win,last_win,current_dft_hpf,last_dft_hpf,window,window2;

           cv::Mat current = input[vector_counter-1]; // Input without CLAHE
           cv::Mat last = input[vector_counter-2];
           cv::Mat currentaltL = input[vector_counter-1];
           cv::Mat currentaltR = inputR[vectorR_counter-1];
           //current.convertTo(current,CV_8UC1,1/257.0); // Need to Include these conversion when not applying CLAHE
           //last.convertTo(last,CV_8UC1,1/257.0);
           //currentaltL.convertTo(currentaltL,CV_8UC1,1/257.0);
           //currentaltR.convertTo(currentaltR,CV_8UC1,1/257.0);


           current.convertTo(current,CV_16UC1,256.0);
           last.convertTo(last,CV_16UC1,256.0);
           currentaltL.convertTo(currentaltL,CV_16UC1,256.0);
           currentaltR.convertTo(currentaltR,CV_16UC1,256.0);

           //hist = calcHistogram(current);       // Histogram Data
           //drawHistogram(hist);
           //cv::Scalar histmean,histstd;
           //cv::meanStdDev(hist,histmean,histstd);
           //std::cout << "Histogram STD: " << histstd << std::endl;

           current.convertTo(current,CV_32FC1,1.0/65535.0); // CV32 required for Phase Correlation Functions
           last.convertTo(last,CV_32FC1,1.0/65535.0);
           currentaltL.convertTo(currentaltL,CV_32FC1,1.0/65535.0);
           currentaltR.convertTo(currentaltR,CV_32FC1,1.0/65535.0);



           cv::remap(current,current, map11, map12, cv::INTER_LINEAR); // Undistort camera images via stereorectify
           cv::remap(last,last, map11, map12, cv::INTER_LINEAR);
           //cv::remap(currentaltL,currentaltL, map11, map12, cv::INTER_LINEAR);
           //cv::remap(currentaltR,currentaltR, map21, map22, cv::INTER_LINEAR);
           //float scalar = 2.0;  // Increase exposure by 50%
           //currentaltR = currentaltR * scalar;



           current = Resize(current); // Resize Images
           last = Resize(last);
           currentaltL = Resize(currentaltL);
           currentaltR = Resize(currentaltR);

           current = HighPass(current, HPkernel);
           last = HighPass(last, HPkernel);
           currentaltL = HighPass(currentaltL, HPkernel);
           currentaltR = HighPass(currentaltR, HPkernel);

           current = applyHistogram(current); // Input & CLAHE
           last = applyHistogram(last);
           currentaltL = applyHistogram(currentaltL);
           currentaltR = applyHistogram(currentaltR);



           //double camera_altitude_phase = getAltitudephase(currentaltL,currentaltR,Q);
           //ShowEpipolarLines(currentaltL,currentaltR,floatcam.Fund);
           //Disparity = getDisparity(currentaltL,currentaltR, Q); //getAltitude vs getAltitudemod
           double camera_altitude = getAltitudemod(currentaltL,currentaltR,Q);




           //rectangle(current,validRoi[0],Scalar(0,0,255), 3, 8); creates rectangle of valid disparity pixels



           //cv::Mat current = Resize(input[array_size-1]);
           //cv::Mat currentR = Resize(inputR[arrayR_size-1]);
           //cv::Mat last = Resize(input[array_size-2]);
           //cv::Mat current_dft, last_dft, current_dft_LP, last_dft_LP, window, window2, current_fix_dft, currentHPF,lastHPF;
           //MatrixXd HPFEigen = HighPass(current);
           //cv::Mat HPF;
           //eigen2cv(HPFEigen,HPF);
           // setting -1 here keeps the destination image with the same source depth
           //Point anchor = Point(-1,1);
           //filter2D(current,currentHPF,-1,HPF,anchor,0,BORDER_DEFAULT);
           //Mat kernel1 = (Mat_<double>(3,3) << 0, -1.0, 0, -1.0, 4.0, -1.0, 0, -1.0, 0);  //Highpass filter (change kernel at some point)
           //Mat kernel1 = (Mat_<double>(3,3) << -1.0, -2.0, -1.0, -2.0, 12.0, -2.0, -1.0, -2.0, -1.0);
           //Mat kernel1 = (Mat_<double>(3,3) << -1.0/9, -1.0/9, -1.0/9, -1.0/9, 8.0/9, -1.0/9, -1.0/9, -1.0/9, -1.0/9);

           current_win = Window(current);
           last_win = Window(last);
           //current_win_hpf = HighPass(current_win);
           //last_win_hpf= HighPass(last_win);

           //cv::imshow("current_win",current_win);



           //Point2d phase = FFT(current,last);    is this needed?
           // Yes, it is. -Dave
           cv::Mat high_pass_test;
           cv::Mat channels[2];
           cv::split(HPkernel, channels);
           cv::Mat HPkernel_vis = channels[0];
           //std::cout << "Image current type: " << image.type() << std::endl;
           //cv::dft(current_win, current_dft_test, 0);
           //filter2D(current_dft_test,current_dft_test, -1 , HPkernel, Point(-1, -1), 0, BORDER_DEFAULT);
           //dft(last,last_dft);
           current_dft = FFT(current_win);
           last_dft = FFT(last_win);
           //std::cout << "Image current type: " << current_dft.type() << std::endl;
           //high_pass_test = HighPass(current_win,HPkernel);
           //last_dft_hpf = HighPass(last_dft);
           //cv::imshow("kernal",HPkernel);
           //filter2D(current,current_dft_test, -1 , HPkernel);
           //cv::idft(current_dft_test,current_dft_test,cv::DFT_SCALE | cv::DFT_REAL_OUTPUT);
           //cv::imshow("current", Resize(current));
           //cv::imshow("current Right", Resize(currentaltR));

           cv::normalize(HPkernel_vis, HPkernel_vis, 0, 255, cv::NORM_MINMAX, CV_8U);
           cv::imshow("Kernel", HPkernel_vis);
           //cv::imshow("HP Test", high_pass_test);
           //cv::imshow("current win", current_win);
           //cv::waitKey(0);
           //cv::imshow("last dft", Resize(last_dft_hpf));

           // Gotcha, here it is
           //Point2d LPcheck = LPFFT(current,last);
           //LP(current_dft,last_dft,current_dft_LP,last_dft_LP);  //STANDARD
           logpolar(current_dft,current_dft_LP2);  //EXPERIMENTAL, MIT LICENSE
           logpolar(last_dft,last_dft_LP2);
           //cv::imshow("LPcurrent",current_dft_LP);
           //cv::imshow("LPcurrentEXPERIMENTAL",current_dft_LP2);
           //cv::Mat current_dft2_LP = FFT(current_dft_LP);
           //cv::Mat last_dft2_LP = FFT(last_dft_LP);
           //cv::imshow("LPcurrent",current_dft_LP);
           //cv::imshow("LPlast",last_dft_LP);

           createHanningWindow(window, current_dft_LP2.size(), CV_32F); //current_dft2_LP
           Point2d phaseLP = cv::phaseCorrelate(current_dft_LP2,last_dft_LP2,window);//current_dft2_LP, last_dft2_LP
           //std::cout << "[Ux, Vy]: " << phaseLP << std::endl;
           Point2d scale_rot = Scale_Rot(phaseLP, current.size()); //return rotate, scale values
           cv::Mat current_fix = Scale_Rot_Fix(current,scale_rot); //return current_fix
           //cv::Mat current_dft_fix = FFT(current_fix);
           createHanningWindow(window2, current.size(), CV_32F);
           double response;
           current_fix_win = Window(current_fix);
           Point2d phase_fix=cv::phaseCorrelate(last_win,current_win,noArray(),&response); //current_dft_fix,last_dft
           //std::cout << "Response Values: " << response << std::endl;
           //cv::imshow("Window",last_win);
           //std::cout << "Phase Fix: " << phase_fix << std::endl;
           //std::cout << "DVL Altitude: " << dvl_altitude << std::endl;
           //std::cout << "DVL X Twist: " << dvl_x << std::endl;
           //std::cout << "DVL Y Twist: " << dvl_y << std::endl;
           //std::cout << "Twist X: " << navsat_x << std::endl;
           //std::cout << "Twist Y: " << navsat_y << std::endl;
           //std::cout << "Twist Processed X: " << odometry_x << std::endl;
           //std::cout << "Twist Processed Y: " << odometry_y << std::endl;
           Phase_Stitch(phase_fix, current_fix, last);//current_fix , current


           //Disparity = getDisparity(currentaltL,currentaltR, Q); //getAltitude vs getAltitudemod
           //cv::Vec3d point = get3D(Disparity, Q);
           cv::Point2d camera_altitude_data = getAltitudephase(currentaltL,currentaltR,Q);
           double camera_altitude_phase = camera_altitude_data.x;
           double stereo_response = camera_altitude_data.y;

           Matrix<double,3,1> Translation = Translate(phase_fix,camera_altitude_phase,floatcam.A); // or use disparity altitude.dvl_altitude
           double Translation_x = Translation[0];
           double Translation_y = Translation[1];
           double Translation_z = Translation[2];//return x,y
           std::cout << "RANGES ALT: " << dvl_altitude << std::endl;
           myfile << timestampimage << ", "
                  << Translation_x << ", "
                  << Translation_y << ", "
                  << camera_altitude_phase << ", "
                  << phase_fix.x << ", "
                  << phase_fix.y << ", "
                  << response << ", "
                  << dvl_altitude_timestamp << ", "
                  << dvl_altitude << ", "
                  << dvl_twist_timestamp << ", "
                  << dvl_x << ", "
                  << dvl_y << ", "
                  << navsat_timestamp << ", "
                  << navsat_x << ", "
                  << navsat_y << ", "
                  << odometry_timestamp << ", "
                  << odometry_x << ", "
                  << odometry_y << ", "
                  << stereo_response << "\n";
           //myfile.close();
           std::cout << "SAVED CSV #############################################" << std::endl;
        //   SURFshow(current,last);
           cv::waitKey(wait);

           if (vector_size >= impostend){

             bag.close();
             std::cout << "bag closed...program end" << std::endl;


             return 0;


           }

           /*unsigned long arraydiff = array_size - arrayR_size;

           if (array_size == arrayR_size){

               Altitude.push_back(Altitude_out);
           }else if (Altitude_size == 0){
               Altitude_out = getAltitude(input[array_size-arraydiff],inputR[arrayR_size]);
               Altitude.push_back(Altitude_out);
           }else {
               Altitude_out = Altitude[array_size-1];
               Altitude.push_back(Altitude_out);
           }
           */
           //Here now, need to write an algo
           //cv::Ptr<cv::StereoBM> stereo = cv::StereoBM::create();
           //cv::Mat disp;
           //stereo->compute(current,currentR,disp);
           //cv::imshow("disparity map",disp);
           //cv::waitKey(0);

           //std::cout <<"X coordinate: " << result.x << std::endl;
           //std::cout <<"Y coordinate: " << result.y << std::endl;






              //cv::imshow("image", image);
              //cv::waitKey(500);
    }
   }
  }




  }



  //bag.close();
  //std::cout << "bag closed...program end" << std::endl;


  //return 0;

}
