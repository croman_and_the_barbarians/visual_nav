#include "visual_nav.h"
#include "phasecorrelatemod.h"

using namespace sensor_msgs;
using namespace message_filters;
using namespace cv;
using namespace Eigen;
using namespace std;

//For Importing Stereo Calibration Data
struct StereoCalData {
  double reprojectionError;
  cv::Mat A;
  cv::Mat B;
  cv::Mat DistA;
  cv::Mat DistB;
  cv::Mat Rot;
  cv::Mat Trans;
  cv::Mat Ess;
  cv::Mat Fund;
};

//Publisher
ros::Publisher visual_nav_twist;

//YAML FILE PATH FOR CALIBRATION DATA
std::string path = ros::package::getPath("visual_nav");
std::string yaml_path = path + "/data/stereo2m.yml";

//Global variables
static int cols = 4112; //1504.0
static int rows = 3008; //2056.0 (Settings for no artifacts, but is slower and less accurate)
static int Cx = rows/2;
static int Cy = cols/2;

StereoCalData readStereoCalData(const std::string& filename) {
  StereoCalData data;
  cv::FileStorage yaml(filename, cv::FileStorage::READ);

  // Read cv::Mat from YAML file
  //fs["matrix"] >> mat;
  if (!yaml.isOpened()) {
         std::cerr << "Failed to open yaml calibration file" << std::endl;
     }

  yaml["reprojectionError"] >> data.reprojectionError;
  yaml["cameraMatrixA"] >> data.A;
  yaml["cameraMatrixB"] >> data.B;
  yaml["distortionCoeffA"] >> data.DistA;
  yaml["distortionCoeffB"] >> data.DistB;
  yaml["rotation"] >> data.Rot;
  yaml["translation"] >> data.Trans;
  yaml["essential"] >> data.Ess;
  yaml["fundamental"] >> data.Fund;

  // Close YAML file
  yaml.release();

  //return data;
  return data;
}

void live_driver_Callback(const sensor_msgs::ImageConstPtr& left_msg, const sensor_msgs::ImageConstPtr& right_msg)
{
    // Start Time, Load Images
    auto start = std::chrono::high_resolution_clock::now();

    // Create Twist Message
    geometry_msgs::TwistStamped twistStamped;

    // Import Images, Assuming 8UC1
    cv::Mat left_image = cv_bridge::toCvShare(left_msg, "32FC1")->image/ 255.0;
    cv::Mat right_image = cv_bridge::toCvShare(right_msg, "32FC1")->image/ 255.0;

    // Process Stereo Calibration Data
    StereoCalData floatcam =readStereoCalData(yaml_path);
    cv::Mat R1,R2,P1,P2,Q, map11,map12,map21,map22;
    Rect validRoi[2];
    cv::stereoRectify(floatcam.A,floatcam.DistA,floatcam.B,floatcam.DistB,cv::Size(4112,3008), floatcam.Rot, floatcam.Trans, R1,R2,P1,P2,Q,0,-1,cv::Size(4112,3008),&validRoi[0],&validRoi[1]);
    cv::initUndistortRectifyMap(floatcam.A,floatcam.DistA,R1,P1,cv::Size(4112,3008), CV_32FC1,map11,map12);
    cv::initUndistortRectifyMap(floatcam.B,floatcam.DistB,R2,P2,cv::Size(4112,3008), CV_32FC1,map21,map22);

    // Apply Undistortion and Rectification Maps
    //cv::remap(left_image,left_image, map11, map12, cv::INTER_LINEAR);
    //cv::remap(right_image,right_image, map11, map12, cv::INTER_LINEAR);

    // Downsample to 1028, 752
    left_image = Resize(left_image);
    right_image = Resize(right_image);

    // Make Highpass Filter Kernel
    cv::Mat HPkernel = generateCosineSquaredHighpassFilter(left_image.cols,left_image.rows,1.0,1.0); //width,height,cutoff freq, order

    // Apply Highpass Filter
    left_image = HighPass(left_image, HPkernel); // Input & CLAHE
    right_image = HighPass(right_image, HPkernel); // Input & CLAHE

    // Apply CLAHE
    left_image = applyHistogram(left_image);
    right_image = applyHistogram(right_image);

    // Show Images, save B&W images to cv::Mat
    //cv::imshow("Left Test", left_image);
    //cv::imshow("Right Test", right_image);

    // Create Hanning Window, Apply Fourier Transform no. 1
    cv::Mat window;
    createHanningWindow(window, left_image.size(), CV_32F);
    cv::Mat left_image_fft;
    left_image_fft = FFTmod(left_image,window);

    // Remap to Log Polar,
    cv::Mat left_image_fft_LP;
    logpolar(left_image_fft,left_image_fft_LP);

    // Create Hanning Window, Apply Fourier Transform no. 2
    cv::Mat LPwindow;
    createHanningWindow(LPwindow,left_image_fft_LP.size(),CV_32F);
    cv::Mat left_image_fft_LP_fft;
    left_image_fft_LP_fft = FFTmod(left_image_fft_LP,LPwindow);

    // Sequential Caching
    static std::vector<cv::Mat> left_image_cache,fft_cache,fft_LP_fft_cache;
    static std::vector<ros::Time> time_cache;
    fft_cache.push_back(left_image_fft);
    fft_LP_fft_cache.push_back(left_image_fft_LP_fft);
    left_image_cache.push_back(left_image);
    time_cache.push_back(left_msg->header.stamp);

    // Process Visual Odometry if Cache Size > 1
    if (fft_cache.size() > 1) {

        // Extract Images, FFTs, Time from Caches
        cv::Mat current = left_image_cache.back();
        cv::Mat last = left_image_cache[left_image_cache.size()-2];
        cv::Mat current_fft = fft_cache.back();
        cv::Mat last_fft = fft_cache[fft_cache.size()-2];
        cv::Mat current_fft_LP_fft = fft_LP_fft_cache.back();
        cv::Mat last_fft_LP_fft = fft_LP_fft_cache[fft_LP_fft_cache.size()-2];
        ros::Time current_time = time_cache.back();  // Get the timestamp of the current image
        ros::Time last_time = time_cache[time_cache.size()-2];     // Get the timestamp of the last image


        // Solving for Rotation & Scale
        createHanningWindow(LPwindow,left_image_fft_LP.size(),CV_32F);
        double rotation_scale_response;
        cv::Point2d phaseLP = phaseCorrelateMod(current_fft_LP_fft,last_fft_LP_fft,&rotation_scale_response);
        Point2d scale_rot = Scale_Rot(phaseLP, current.size());

        // De-Rotate & De-Scale Current Image, Window & Fourier Transform
        cv::Mat current_fix = Scale_Rot_Fix(current,scale_rot);
        cv::Mat current_fix_fft = FFTmod(current_fix,window);

        // Calculate Translation in Pixels, Response Value
        double translation_response;
        Point2d phase_fix = phaseCorrelateMod(last_fft, current_fix_fft,&translation_response);
        Phase_Stitch(phase_fix, current_fix, last);//current_fix , current

        // Right Image FFT
        cv::Mat right_image_fft;
        right_image_fft = FFTmod(right_image,window);

        // Calculate Altitude, Response Value
        cv::Point2d camera_altitude_data = getAltitudephaselive(current_fft,right_image_fft,Q);
        double camera_altitude_phase = camera_altitude_data.x;
        double altitude_response = camera_altitude_data.y;

        // Scale Translation
        Matrix<double,3,1> Translation = Translate(phase_fix,camera_altitude_phase,floatcam.A);

        // Calculate Time, Velocity
        ros::Duration timediff = current_time - last_time;
        double timediff_seconds = timediff.toSec();
        double Velocity_x = Translation[0] * timediff_seconds;
        double Velocity_y = Translation[1] * timediff_seconds;
        double Velocity_z = Translation[2] * timediff_seconds;


        // Fill in the data here
        twistStamped.header.stamp = ros::Time::now();
        twistStamped.twist.linear.x = Velocity_x;
        twistStamped.twist.linear.y = Velocity_y;
        twistStamped.twist.linear.z = Velocity_z;
        // Fill other fields as necessary

        // Publish the message
        visual_nav_twist.publish(twistStamped);

        //std::cout
        //std::cout << "STEREO ALT: " << phase_depth << std::endl;


        // Remove Oldest FFT From Cache
        fft_cache.erase(fft_cache.begin());
        fft_LP_fft_cache.erase(fft_LP_fft_cache.begin());
        time_cache.erase(time_cache.begin());

    }
    // End Time, Post Results
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << std::endl;
    //cv::waitKey(20);
}







int main(int argc, char** argv)
{
    ros::init(argc, argv, "live_driver");
    ros::NodeHandle live_driver;
    std::cout << "Starting node_live_driver..." << std::endl;

    // Initialize the publisher
    visual_nav_twist = live_driver.advertise<geometry_msgs::TwistStamped>("visual_nav_twist", 1);

    // Create subscribers for the stereo left and right topics
    Subscriber<Image> left_sub(live_driver, "/stereo/left/image_raw", 10);
    Subscriber<Image> right_sub(live_driver, "/stereo/right/image_raw", 10);

    // Define the synchronization policy
    typedef sync_policies::ApproximateTime<Image, Image> SyncPolicy;
    Synchronizer<SyncPolicy> sync(SyncPolicy(10), left_sub, right_sub);
    sync.registerCallback(boost::bind(&live_driver_Callback, _1, _2));


    // Spin and process callbacks
    ros::spin();

    return 0;
}






