#include <ros/ros.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>
#include <geometry_msgs/TwistStamped.h>
//This ROS node changes TwistwithCovarianceStamped to TwistStamped for view in RViz
//You can modify this code for your own uses by changing the hdg and pos subscriber messages on lines 52 & 53
ros::Publisher twistPub_hdg;
ros::Publisher twistPub_pos;

void Publish_hdg(ros::Publisher twistPub_hdg, geometry_msgs::TwistStamped twistMsg_hdg){
  twistPub_hdg.publish(twistMsg_hdg);
  std::cout << "Publishing hdg..." << std::endl;
}

void Publish_pos(ros::Publisher twistPub_pos, geometry_msgs::TwistStamped twistMsg_pos){
  twistPub_pos.publish(twistMsg_pos);
  std::cout << "Publishing pos..." << std::endl;
}

void twistWithCovarianceCallback_hdg(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& twistWithCovarianceMsg) {
  // Extract the twist component from TwistWithCovarianceStamped message
  geometry_msgs::TwistStamped twistMsg_hdg;
  twistMsg_hdg.header = twistWithCovarianceMsg->header;
  twistMsg_hdg.twist = twistWithCovarianceMsg->twist.twist;
  std::cout << "I heard a hdg message: " << twistMsg_hdg << std::endl;
  // Publish the TwistStamped message
  Publish_hdg(twistPub_hdg,twistMsg_hdg);

}

void twistWithCovarianceCallback_pos(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& twistWithCovarianceMsg) {
  // Extract the twist component from TwistWithCovarianceStamped message
  geometry_msgs::TwistStamped twistMsg_pos;
  twistMsg_pos.header = twistWithCovarianceMsg->header;
  twistMsg_pos.twist = twistWithCovarianceMsg->twist.twist;
  std::cout << "I heard a pos message: " << twistMsg_pos << std::endl;
  // Publish the TwistStamped message
  Publish_pos(twistPub_pos,twistMsg_pos);


}



int main(int argc, char** argv) {
  ros::init(argc, argv, "twist_converter");
  std::cout << "Starting Program... " << std::endl;
  ros::NodeHandle twistcov2twiststamp;

  twistPub_hdg = twistcov2twiststamp.advertise<geometry_msgs::TwistStamped>("twist_stamped_hdg", 1);
  twistPub_pos = twistcov2twiststamp.advertise<geometry_msgs::TwistStamped>("twist_stamped_pos", 1);

  ros::Subscriber twistWithCovarianceSub_hdg = twistcov2twiststamp.subscribe("/nav/sensors/navsat/ubx_hdg/twist_enu", 1, twistWithCovarianceCallback_hdg);
  ros::Subscriber twistWithCovarianceSub2_pos = twistcov2twiststamp.subscribe("/nav/sensors/navsat/ubx_pos/twist_enu", 1, twistWithCovarianceCallback_pos);

  ros::spin();

  return 0;
}
