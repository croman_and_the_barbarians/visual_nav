#ifndef TEMPLATE_CLASS_H
#define TEMPLATE_CLASS_H

#include <std_msgs/String.h>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <ros/package.h>
//#include <yaml-cpp/yaml.h> will use OpenCV libraries for reading yaml,XML
//#include <tinyxml.h>
//#include "visual_nav.h"
#include <ds_sensor_msgs/Dvl.h>
#include <std_msgs/String.h>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include <eigen3/unsupported/Eigen/MatrixFunctions>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv4/opencv2/core.hpp>
#include <opencv4/opencv2/opencv.hpp>
#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/core/eigen.hpp>
#include <opencv4/opencv2/calib3d.hpp>
#include <opencv4/opencv2/ximgproc/disparity_filter.hpp>
#include <opencv4/opencv2/viz/viz3d.hpp>
#include <opencv4/opencv2/features2d.hpp>
#include <opencv4/opencv2/xfeatures2d/nonfree.hpp>
#include <opencv4/opencv2/xfeatures2d.hpp>
#include <opencv4/opencv2/stitching.hpp>
#include <stdio.h>
#include <cmath>
#include <chrono>
#include <string>
#include <std_msgs/Float64.h>
#include <image_proc/DebayerConfig.h>
#include <image_proc/advertisement_checker.h>
#include <image_proc/processor.h>
#include <image_proc/CropDecimateConfig.h>
#include <image_proc/RectifyConfig.h>
#include <image_proc/ResizeConfig.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Vector3.h>
#include <sensor_msgs/Range.h>
#include <sensor_msgs/Image.h>
#include <nav_msgs/Odometry.h>
#include <fstream>

using namespace cv;
using namespace cv::xfeatures2d;
using namespace Eigen;
using namespace std;


//StereoCalData readStereoCalData(const std::string& filename);
cv::Mat generateCosineSquaredHighpassFilter(int width, int height, float cutoffFrequency, float order);
cv::Mat Resize(cv::Mat image_in);
void onMouse(int event,int x, int y,int flags, void* userdata);
void ShowEpipolarLines(cv::Mat &img_left, cv::Mat &img_right, cv::Mat Fund);
void SURFshow(cv::Mat &img1,cv::Mat &img2);
bool ImageSync(vector<unsigned long int> number_tracker);
cv::Mat Window(cv::Mat image_in);
cv::Mat HighPass(cv::Mat image_in, cv::Mat kernel);
cv::Mat MakeSquare(cv::Mat image_in);
void showImagePairs(cv::Mat &img1,cv::Mat &img2,Point2d &phase_Fix); //& for *
cv::Mat FFT(cv::Mat I);
float logpolar(Mat& src, Mat& dst);
void LP(cv::Mat image_1, cv::Mat image_2, cv::Mat &current_dft_LP, cv::Mat &last_dft_LP);
Point2d Scale_Rot(Point2d phaseLP, cv::Size imagesize);
cv::Mat Scale_Rot_Fix(cv::Mat image_in, Point2d scale_rot);
void Phase_Stitch(Point2d phase_Fix,cv::Mat current_fix, cv::Mat last);
cv::Mat calcHistogram(cv::Mat image_in);
void drawHistogram(cv::Mat hist);
cv::Mat applyHistogram(cv::Mat image_in);
Point2d getAltitudephase(cv::Mat currentaltL, cv::Mat currentaltR, Mat Q);
Point2d getAltitudephaselive(cv::Mat currentLfft, cv::Mat currentRfft, Mat Q);
double getAltitudemod(cv::Mat imageL, cv::Mat imageR, Mat Q);
cv::Mat getDisparity(cv::Mat imageL, cv::Mat imageR, Mat Q);
cv::Vec3d get3D(cv::Mat disp,cv::Mat Q);
Matrix<double,3,1> Translate(Point2d phase_fix, double depth,cv::Mat A);

//void TemplateClass::spin();

class TemplateClass
{
public:


  TemplateClass();

  void chatterCallback(const std_msgs::String::ConstPtr& msg);
  /*!
   * \brief spin is degined to run the node continuously
   */
  void spin();
  /*!
   * \brief spin_once is intended to run the loop just once
   */
  void spinOnce();

protected:
  ros::NodeHandlePtr ros_node_ptr_;
  ros::Subscriber hello_sub_;

};

#endif // TEMPLATE_CLASS_H
