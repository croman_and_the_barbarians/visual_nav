#ifndef PHASECORRELATEMOD_H
#define PHASECORRELATEMOD_H

#include "visual_nav.h"

using namespace cv;
using namespace std;

static void magSpectrums( InputArray _src, OutputArray _dst);
void divSpectrums( InputArray _srcA, InputArray _srcB, OutputArray _dst, int flags, bool conjB);
static void fftShift(InputOutputArray _out);
static Point2d weightedCentroid(InputArray _src, cv::Point peakLocation, cv::Size weightBoxSize, double* response);
cv::Point2d phaseCorrelateMod(cv::Mat FFT1, cv::Mat FFT2, double* response);
cv::Mat FFTmod(InputArray _src, InputArray _window);
#endif // PHASECORRELATEMOD_H
