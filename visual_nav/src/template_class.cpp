#include "visual_nav.h"

using namespace cv;
using namespace Eigen;
using namespace std;

TemplateClass::TemplateClass()
{
  /* I decided to use a pointer to my ros node no I need to allocate memory
   * also,  I told it to use the the node's namespac.pecified by ~ just like linux directories)
   * basically all that means is all broadcast topics will be sent as "node_name/topic_name" unless
   * otherwise specified. */
  ros_node_ptr_.reset(new ros::NodeHandle("~"));
  //hello_sub_ = ros_node_ptr_->subscribe("/chatter",1,&TemplateClass::chatterCallback,this);
}

void TemplateClass::chatterCallback(const std_msgs::String::ConstPtr &msg){
  ROS_INFO("I heard: [%s]", msg->data.c_str());
}

void TemplateClass::spinOnce(){
  //ROS_INFO("I don't do anything yet.  Jake is going to make me send a message");

  // after we spin once we need to tell ros to do the same
  ros::spinOnce();
}


