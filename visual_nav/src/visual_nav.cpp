#include "visual_nav.h"
#include "phasecorrelatemod.h"

using namespace std;





cv::Mat Resize(cv::Mat image_in){
  cv::Mat resize;
  cv::Size2d dsize;
  dsize = cv::Size2d(1028,752); //width,height 1028,752
  //dsize = cv::Size2d(image_in.cols*result_scale,image_in.rows*result_scale); //lines 70, 25 and 26 change too
  cv::resize(image_in, resize, dsize);
  return resize;
}

void onMouse(int event,int x, int y,int flags, void* userdata){
  std::vector<cv::Point2f>* points = (std::vector<cv::Point2f>*) userdata;
    if (event == cv::EVENT_LBUTTONDOWN) {
            cv::Point2f click = cv::Point2f(x, y);
            points->emplace_back(click);
            std::cout << "Added point: " << click << std::endl;
    }
  }

cv::Mat generateCosineSquaredHighpassFilter(int width, int height, float cutoffFrequency, float order = 1.0) {

  cv::Mat kernel(height, width, CV_32FC2, cv::Scalar(0,0));
  float centerX = static_cast<float>(width) / 2.0f;
  float centerY = static_cast<float>(height) / 2.0f;

  for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
          float deltaX = (x - centerX) / (width / 2.0);
          float deltaY = (y - centerY) / (height / 2.0);
          float distance = std::sqrt(deltaX * deltaX + deltaY * deltaY);
          //float h = (distance >= cutoffFrequency) ? 1.0 : std::pow(std::cos((M_PI * distance) / (2.0 * cutoffFrequency)), 2 * order);
          float h = (distance >= cutoffFrequency) ? 1.0 : (1 - std::pow(std::cos((M_PI * distance) / (2.0 * cutoffFrequency)), 2 * order));
          kernel.at<cv::Vec2f>(y, x) = cv::Vec2f(h, 0);
      }
  }

  return kernel;
}



void ShowEpipolarLines(cv::Mat &img_left, cv::Mat &img_right, cv::Mat Fund){
  // Compute epipolar lines for left and right images
  std::vector<cv::Point2f> ptsLeft, ptsRight;
  cv::namedWindow("Left Image", cv::WINDOW_NORMAL);
  cv::imshow("Left Image", img_left);
  cv::namedWindow("Right Image", cv::WINDOW_NORMAL);
  cv::imshow("Right Image", img_right);
  cv::setMouseCallback("Left Image", onMouse, static_cast<void*>(&ptsLeft));
  cv::setMouseCallback("Right Image", onMouse, static_cast<void*>(&ptsRight));
  cv::waitKey(0);


  std::vector<cv::Vec3f> lines_left, lines_right;
  cv::computeCorrespondEpilines(ptsLeft, 1, Fund, lines_right);
  cv::computeCorrespondEpilines(ptsRight, 2, Fund, lines_left);

  // Display stereo pair and epipolar lines
  cv::Mat stereo_pair(img_left.rows, img_left.cols*2, CV_8UC1);
  cv::hconcat(img_left, img_right, stereo_pair);

  for(int i = 0; i < lines_left.size(); i++) {
      cv::Scalar color = cv::Scalar(255, 255, 255);
      cv::line(stereo_pair.colRange(0, img_left.cols), cv::Point(0, -lines_left[i][2]/lines_left[i][1]), cv::Point(img_left.cols, -(lines_left[i][2]+lines_left[i][0]*img_left.cols)/lines_left[i][1]), color);
      cv::line(stereo_pair.colRange(img_left.cols, stereo_pair.cols), cv::Point(0, -lines_right[i][2]/lines_right[i][1]), cv::Point(img_right.cols, -(lines_right[i][2]+lines_right[i][0]*img_right.cols)/lines_right[i][1]), color);
  }

  cv::imshow("Stereo Pair with Epipolar Lines", stereo_pair);
  cv::waitKey(0);
}

void SURFshow(cv::Mat &img1,cv::Mat &img2){

  img1.convertTo(img1,CV_8UC1,255.0);
  img2.convertTo(img2,CV_8UC1,255.0);
  double minHessian = 400;
  Ptr<SURF> detector = SURF::create(minHessian, 4,3,false,false);
  //Ptr<SURF> detector;
  std::vector<KeyPoint> keypoints1, keypoints2;
  Mat descriptors1, descriptors2;
  detector->detectAndCompute( img1, noArray(), keypoints1, descriptors1 );
  detector->detectAndCompute( img2, noArray(), keypoints2, descriptors2 );
  //-- Step 2: Matching descriptor vectors with a FLANN based matcher
  // Since SURF is a floating-point descriptor NORM_L2 is used
  Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);
  std::vector< std::vector<DMatch> > knn_matches;
  matcher->knnMatch( descriptors1, descriptors2, knn_matches, 2 );
  //-- Filter matches using the Lowe's ratio test
  const float ratio_thresh = 0.7f;
  std::vector<DMatch> good_matches;
  for (size_t i = 0; i < knn_matches.size(); i++)
  {
      if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance)
      {
         good_matches.push_back(knn_matches[i][0]);
      }
  }
  //-- Draw matches
  Mat img_matches;
  drawMatches( img1, keypoints1, img2, keypoints2, good_matches, img_matches, Scalar::all(-1),
               Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::DEFAULT );
  //-- Show detected matches
  double factor = .65;
  cv::Size2d dsize = cv::Size2d(img_matches.cols*factor, img_matches.rows*factor);
  cv::resize(img_matches,img_matches,dsize);
  cv::imshow("Good Matches", img_matches );

}




bool ImageSync(vector<unsigned long int> number_tracker){
  unsigned long int number_tracker_current = number_tracker[number_tracker.size()];
  unsigned long int number_tracker_last = number_tracker[number_tracker.size()-1];
  if (number_tracker.size()>1){
      if (number_tracker_current == number_tracker_last){
          return false;
      }
  }
  return true;
}

cv::Mat Window(cv::Mat image_in){
  cv::Mat window;
  cv::Mat result = cv::Mat::zeros(image_in.rows,image_in.cols,CV_32FC1);
  createHanningWindow(window, image_in.size(), CV_32FC1);
  //std::cout << "Image window type: " << window.type() << std::endl;
  //std::cout << "Image in type: " << image_in.type() << std::endl;
  accumulateProduct(image_in,window,result,noArray());
  // maybe add accumulateWeighted
  return result;
}
cv::Mat HighPass(cv::Mat image_in, cv::Mat kernel){ //MatrixXd if ug commended out algo                //MatrixXd
  cv::Mat dft_image;
  cv::dft(image_in, dft_image, cv::DFT_COMPLEX_OUTPUT);

  // Create a cosine squared highpass filter kernel
  //cv::Mat kernel = create_cosine_squared_kernel(image.rows, 0.2f);

  // Apply the filter to the Fourier Transform of the input image
  cv::Mat filtered_dft;
  //std::cout << "Image A type: " << dft_image.type() << std::endl;
  //std::cout << "Image B type: " << kernel.type() << std::endl;
  cv::mulSpectrums(dft_image, kernel, filtered_dft, 0);

  // Compute the inverse Fourier Transform of the filtered image
  cv::Mat filtered_image;
  cv::idft(filtered_dft, filtered_image, cv::DFT_SCALE | cv::DFT_REAL_OUTPUT);
  //cv::normalize(filtered_image, filtered_image, 0, 1, cv::NORM_MINMAX, CV_32F);

  // Display the filtered image
  //cv::Mat channels[2];
  //cv::split(filtered_dft, channels);
  //cv::Mat filtered_dft_vis = channels[0];
  //cv::imshow("Filtered DFT", filtered_dft_vis);
  //cv::imshow("filtered Image", filtered_image);
  //cv::imshow("original", image_in);
  //cv::waitKey(0);

  return filtered_image;
}

/*
cv::Mat MakeSquare(cv::Mat image_in){ //WORKS ONLY FOR LANDSCAPE
cv::Mat result;
//int newY = (cols-rows)/4; // First strategy was to pad image, will now try to crop.. some incorrect math here, need adj for scale factor
//copyMakeBorder(image_in, result, newY, newY, 0, 0, BORDER_CONSTANT);
int startX = ((cols*result_scale)-(rows*result_scale))/2;
int endX = startX + (cols*result_scale);
result = image_in(Range(1,rows*result_scale),Range(startX,endX));
cv::imshow("Square ", result);
  return result;
}
*/

void showImagePairs(cv::Mat &img1,cv::Mat &img2,Point2d &phase_Fix) {

    std::vector<cv::Mat> channels;
    cv::Mat imgPair;
    channels.push_back(img2);
    channels.push_back(img1);
    channels.push_back(img2);
    //std::cout << "Image 1 type: " << img1.type() << std::endl;
    //std::cout << "Image 2 type: " << img2.type() << std::endl;
    cv::merge(&channels[0], channels.size(), imgPair);
    imgPair = Resize(imgPair);
    cv::imshow("imgPair",Resize(imgPair));






    ////////////////////////

}




cv::Mat FFT(cv::Mat I){
   //double * response = 0;
   cv::Mat padded;
   //cv::Mat I = cv::Mat::zeros(image_in.rows,image_in.cols,CV_32F);
   //createHanningWindow(window, image_in.size(), CV_32F);
   //accumulateProduct(image_in,window,I,noArray());
   //cv::imshow("windowed", Resize(I));
   //filter2D(I, I, -1 , kernel1, Point(-1, -1), 0, BORDER_DEFAULT);
   //cv::boxFilter(I,I,-1, Size(45,45), Point(-1,-1), 1, BORDER_DEFAULT);
   //cv::imshow("windowed,filtered", Resize(I));
   //cv::waitKey(0);

   int m = getOptimalDFTSize( I.rows );
   int n = getOptimalDFTSize( I.cols ); // on the border add zero values
   copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, BORDER_CONSTANT, Scalar::all(0));

   Mat planes[] = {Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F)};
   Mat complexI;
   merge(planes, 2, complexI);         // Add to the expanded another plane with zeros
   dft(complexI, complexI);
   // compute the magnitude and switch to logarithmic scale
   // => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
   split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
   magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
   Mat magI = planes[0];

   magI += Scalar::all(1);                    // switch to logarithmic scale
   log(magI, magI);

   // crop the spectrum, if it has an odd number of rows or columns
   magI = magI(Rect(0, 0, magI.cols & -2, magI.rows & -2));

   // rearrange the quadrants of Fourier image  so that the origin is at the image center
   int cx = magI.cols/2;
   int cy = magI.rows/2;

   Mat q0(magI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
   Mat q1(magI, Rect(cx, 0, cx, cy));  // Top-Right
   Mat q2(magI, Rect(0, cy, cx, cy));  // Bottom-Left
   Mat q3(magI, Rect(cx, cy, cx, cy)); // Bottom-Right

   Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
   q0.copyTo(tmp);
   q3.copyTo(q0);
   tmp.copyTo(q3);

   q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
   q2.copyTo(q1);
   tmp.copyTo(q2);

   normalize(magI, magI, 0, 1, NORM_MINMAX); // Transform the matrix with float values into a
                                              // viewable image form (float between values 0 and 1).

   return magI;
}

float logpolar(Mat& src, Mat& dst)
{
    float radii = src.cols;
    float angles = src.rows;
    Point2f center(src.cols / 2, src.rows / 2);
    float d = norm(Vec2f(src.cols - center.x, src.rows - center.y));
    float log_base = pow(10.0, log10(d) / radii);
    float d_theta = CV_PI / (float)angles;
    float theta = CV_PI / 2.0;
    float radius = 0;
    Mat map_x(src.size(), CV_32FC1);
    Mat map_y(src.size(), CV_32FC1);
    for (int i = 0; i < angles; ++i)
    {
        for (int j = 0; j < radii; ++j)
        {
            radius = pow(log_base, float(j));
            float x = radius * sin(theta) + center.x;
            float y = radius * cos(theta) + center.y;
            map_x.at<float>(i, j) = x;
            map_y.at<float>(i, j) = y;
        }
        theta += d_theta;
    }
    remap(src, dst, map_x, map_y, cv::INTER_LINEAR, BORDER_CONSTANT, Scalar(0, 0, 0));
    return log_base;
}

void LP(cv::Mat image_1, cv::Mat image_2, cv::Mat &current_dft_LP, cv::Mat &last_dft_LP){

  Point2f center_1( float(image_1.cols) / 2, float(image_1.rows) / 2 );
  Point2f center_2( float(image_2.cols) / 2, float(image_2.rows) / 2 );
  //double *response = 0;
  double Max = min(image_1.cols/2, image_1.rows/2);//Cy,Cx


  //cv::logPolar(image_1, image_1_LP, center_1, M, flags);
  //cv::logPolar(image_2, image_2_LP, center_2, M, flags);
  cv::warpPolar(image_1, current_dft_LP, Size(), center_1, Max, WARP_POLAR_LOG);
  cv::warpPolar(image_2, last_dft_LP, Size(), center_2, Max, WARP_POLAR_LOG);
  cv::imshow("LP",Resize(current_dft_LP));
  //int type = image_1_LP_64.type();
  //cv::Mat window;                                                //Hanning Window applied successfully
  //createHanningWindow(window, image_1_LP.size(), CV_32F);
  //Point2d result = cv::phaseCorrelate(image_1_LP,image_2_LP);    //, window, response);
  //std::cout << "[Ux, Vy]: " << result << std::endl;
}

Point2d Scale_Rot(Point2d phaseLP, cv::Size imagesize){
  Point2d scale_rot;
  Point2d scale_rot_out;
  double Ux = phaseLP.x;
  double Vy = phaseLP.y;
  //From LPFFT, UX, VY
  int rows = imagesize.height;
  int cols = imagesize.width;
  int Cx = rows/2;
  int Cy = cols/2;
  scale_rot.x = sqrt(pow(Ux-Cx, 2.0)+pow(Vy-Cy, 2.0))*(rows/(log10(rows/2.0))); //SCALE
  double Scale1 = exp((scale_rot.x/rows)*(log10(rows/2.0)));
  double Scale2 = exp(((-rows-scale_rot.x)/rows)*(log10(rows/2.0)));
  if (abs(log10(Scale1)) < abs(log10(Scale2))){
    scale_rot_out.x = Scale1;
  }
  else {
    scale_rot_out.x = Scale2;
  }

  scale_rot.y = ((atan((Vy-Cy)/(Ux-Cx))*(180.0/3.141592653589793238463))*(cols/360.0)); //ROTATION
  double Rot1 = (360.0/cols) * scale_rot.y;
  double Rot2 = (360.0/cols) * (scale_rot.y - cols);//Vy or scale_rot.y
  if (abs(Rot1) < abs(Rot2)) {
    scale_rot_out.y = Rot1;
  }
  else {
    scale_rot_out.y = Rot2;
  }
/*
  scale_rot.x = sqrt(pow(Ux-Cx, 2.0)+pow(Vy-Cy, 2.0))*(rows/(log10(rows/2.0))); //SCALE
  double Scale1 = exp((scale_rot.x/rows)*(log10(rows/2.0)));
  double Scale2 = exp(((-rows-scale_rot.x)/rows)*(log10(rows/2.0)));
  if (abs(log10(Scale1)) < abs(log10(Scale2))){
    scale_rot_out.x = Scale1;
  }
  else {
    scale_rot_out.x = Scale2;
  }

  scale_rot.y = ((atan((Vy-Cy)/(Ux-Cx))*(180.0/3.141592653589793238463))*(cols/360.0)); //ROTATION
  double Rot1 = (360.0/cols) * scale_rot.y;
  double Rot2 = (360.0/cols) * (scale_rot.y - cols);//Vy or scale_rot.y
  if (abs(Rot1) < abs(Rot2)) {
    scale_rot_out.y = Rot1;
  }
  else {
    scale_rot_out.y = Rot2;
  }
  */

  //std::cout << "[Scale, Rotation]: " << scale_rot_out << std::endl;
  return scale_rot_out;
}

cv::Mat Scale_Rot_Fix(cv::Mat image_in, Point2d scale_rot){
  cv::Mat result;
  double scale = scale_rot.x;
  double rot = -scale_rot.y + 55; //+50
  double rot_radians = abs(rot)*(M_PI/180);
  cv::Size newsize;

  double result_scale = .5; //Check this
  int rows = image_in.rows;
  int cols = image_in.cols;

  newsize.height = (result_scale*cols*(rot_radians))+(result_scale*rows*cos(rot_radians));
  newsize.width  = (result_scale*cols*cos(rot_radians))+(result_scale*rows*(rot_radians));

  if (newsize.height % 2 != 0){
    newsize.height = newsize.height+1;
  }
  if (newsize.width % 2 !=0){
    newsize.width = newsize.width+1;
  }
  Point2d center(image_in.cols/2,image_in.rows/2);
  //std::cout << "center: " << center << std::endl;
  //std::cout << "Rot new Size: " << newsize << std::endl;
  //std::cout << "Rot:" << rot << std::endl;
  cv::Mat rotation_matrix = getRotationMatrix2D(center, rot, 1.0);
  //std::cout << "Rot Matrix: " << rotation_matrix << std::endl;//rotation input here, + 45?
  //double zero2 = rotation_matrix.at<double>(0,2);
  //double one2 = rotation_matrix.at<double>(1,2);
  //rotation_matrix.at<double>(0,2) = zero2 + (newsize.width/2)-center.x;
  //rotation_matrix.at<double>(1,2) = one2 +(newsize.height/2)-center.y;
  warpAffine(image_in, result, rotation_matrix, image_in.size());
  //cv::imshow("fixed", result); //result
  //cv::waitKey(0);
  //Output rotated & scaled image
  //cv::imshow("image", result_1920);
  //cv::waitKey(0);
  return result;
}

void Phase_Stitch(Point2d phase_Fix,cv::Mat current_fix, cv::Mat last){
cv::Mat result;
cv::Size diff = current_fix.size() - last.size();
int current_fix_padY,current_fix_padX,last_padY,last_padX;
int rows = last.rows;
int cols = last.cols;
int Cx = rows/2;
int Cy = cols/2;
current_fix_padY = Cy;
current_fix_padX = Cx;
last_padY = (Cy+(diff.height/2));
last_padX = (Cx+(diff.width/2));

//std::cout << "current_fix pre size: " << current_fix.size << std::endl;
//std::cout << "last pre size: " << last.size << std::endl;
//copyMakeBorder(current_fix, current_fix, current_fix_padY, current_fix_padY, current_fix_padX, current_fix_padX, BORDER_CONSTANT);
//copyMakeBorder(last, last, last_padY, last_padY, last_padX, last_padX, BORDER_CONSTANT);
copyMakeBorder(current_fix, current_fix, 510, 510, 510, 510, BORDER_CONSTANT);
copyMakeBorder(last, last, 510, 510, 510, 510, BORDER_CONSTANT);

//std::cout << "current_fix size: " << current_fix.size << std::endl;
//std::cout << "last size: " << last.size << std::endl;
//std::cout << "diff size: " << diff << std::endl;

cv::Mat trans_matrix = (Mat_<double>(2,3) << 1, 0, -phase_Fix.x, 0, 1,  -phase_Fix.y); //-phase_Fix.y
warpAffine(current_fix,result, trans_matrix, current_fix.size());

// Trying something different here ...
//cv::addWeighted(result, 1, last, 1, 0, result);

//showImagePairs(result,last,phase_Fix);


//cv::imshow("translated", result);
}

cv::Mat calcHistogram(cv::Mat image_in){
  const cv::Mat &image_hist = image_in;
  cv::Mat hist;
  const int histSize = 256;
  float range[] ={0,256};
  const float *ranges = {range};
  cv::calcHist(&image_hist,1,nullptr,cv::Mat(),hist,1,&histSize,&ranges);
  return hist;
}

void drawHistogram(cv::Mat hist){
  //std::cout << "Hist: " << hist << std::endl;
  const int histSize = 256;
  int hist_w = 512, hist_h = 512;
  cv::Mat histImage(hist_w,hist_h,CV_8UC1,Scalar(0,0,0));
  int bin_w = cvRound( (double) hist_w/histSize );
  normalize(hist, hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );

  for( int i = 1; i < histSize; i++ )
      {
          line( histImage, Point( bin_w*(i-1), hist_h - cvRound(hist.at<float>(i-1)) ),
                Point( bin_w*(i), hist_h - cvRound(hist.at<float>(i)) ),
                Scalar( 255, 0, 0), 2, 8, 0  );
      }
  cv::imshow("Histogram", histImage);
}



cv::Mat applyHistogram(cv::Mat image_in){
//cv::cvtColor(image_in,image_in,COLOR_BayerGR2GRAY)
cv::Mat image_src;
//cv::saturate_cast

image_in.convertTo(image_src,CV_8UC1, 255.0);

//cv::waitKey(0);
cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE(6.0,Size(32,32));
clahe->apply(image_src,image_src);
image_src.convertTo(image_src,CV_32FC1,1.0/255.0);
//cv::imshow("histogram test",image_src);
return image_src;
}

//cv::Mat Debayer(cv::Mat image_in){
//  cv::


//}
Point2d getAltitudephase(cv::Mat currentaltL, cv::Mat currentaltR, Mat Q){
//float scalar = 1.2;  // Increase exposure by 50%
//currentaltR = currentaltR * scalar;
cv::imshow("phase_depth_init_L", currentaltL);
cv::imshow("phase_depth_init_R", currentaltR);
// Increase the exposure by multiplying the image by a scalar value

Point2d phase_depth;
cv::Mat currentaltL_trans;
cv::Mat currentaltL_win = Window(currentaltL);
cv::Mat currentaltR_win = Window(currentaltR);
double stereo_response;
Point2d phase_fix = cv::phaseCorrelate(currentaltL,currentaltR,noArray(),&stereo_response);
copyMakeBorder(currentaltL, currentaltL, 510, 510, 510, 510, BORDER_CONSTANT);
copyMakeBorder(currentaltR, currentaltR, 510, 510, 510, 510, BORDER_CONSTANT);


//std::cout << "current_fix size: " << current_fix.size << std::endl;
//std::cout << "last size: " << last.size << std::endl;
//std::cout << "diff size: " << diff << std::endl;

cv::Mat trans_matrix = (Mat_<double>(2,3) << 1, 0, phase_fix.x, 0, 1, phase_fix.y); //-phase_Fix.y
warpAffine(currentaltL,currentaltL_trans, trans_matrix, currentaltL.size());

// Trying something different here ...
//cv::addWeighted(result, 1, last, 1, 0, result);
std::vector<cv::Mat> channels;
cv::Mat imgPair;
channels.push_back(currentaltR);
channels.push_back(currentaltL_trans);
channels.push_back(currentaltR);
//std::cout << "Image 2 type: " << img2.type() << std::endl;
phase_depth.x = ((100.189469 * 2698.8) / abs(phase_fix.x*4))/1000;
phase_depth.y = stereo_response;

std::cout << "STEREO ALT: " << phase_depth << std::endl;
cv::merge(&channels[0], channels.size(), imgPair);
imgPair = Resize(imgPair);
cv::imshow("stereo",Resize(imgPair));
//showImagePairs(result,last,phase_Fix);
return phase_depth;
}

Point2d getAltitudephaselive(cv::Mat currentLfft, cv::Mat currentRfft, Mat Q){
//float scalar = 1.2;  // Increase exposure by 50%
//currentaltR = currentaltR * scalar;
//cv::imshow("phase_depth_init_L", currentaltL);
//cv::imshow("phase_depth_init_R", currentaltR);
// Increase the exposure by multiplying the image by a scalar value

Point2d phase_depth;
cv::Mat currentaltL_trans;

double altitude_response;
cv::Point2d phase_fix = phaseCorrelateMod(currentLfft,currentRfft,&altitude_response);

phase_depth.x = ((100.189469 * 2698.8) / abs(phase_fix.x*4))/1000;
phase_depth.y = altitude_response;

return phase_depth;
}

double getAltitudemod(cv::Mat imageL, cv::Mat imageR, Mat Q){

  const int minDisparity = 0;
  const int numDisparities = 80;//0 , 80
  const int blockSize = 7;//21, 7,3
  const int P1 = 8* imageL.channels() * blockSize*blockSize;
  const int P2 = 32* imageL.channels() * blockSize*blockSize;
  const int disp12MaxDiff = -1;
  const int preFilterCap = 0;
  const int uniquenessRatio = 55;
  const int speckleWindowSize = 10;
  const int speckleRange = 5;
  double avg_depth;
  //cv::Mat_ <float> Qfloat = Q;

  cv::Ptr<cv::StereoMatcher> pStereo = cv::StereoSGBM::create	(	minDisparity,
                                                                numDisparities,
                                                                blockSize,
                                                                P1,
                                                                P2,
                                                                disp12MaxDiff,
                                                                preFilterCap,
                                                                uniquenessRatio,
                                                                speckleWindowSize,
                                                                speckleRange,
                                                                StereoSGBM::MODE_SGBM
                                                                )		;


  cv::ximgproc::createDisparityWLSFilter(pStereo);
  cv::Mat disp,dispvis,dispmath;

  imageL.convertTo(imageL,CV_8UC1,255.0);
  imageR.convertTo(imageR,CV_8UC1,255.0);

  pStereo->compute(imageL,imageR,disp);
  cv::Mat stereo_pair(imageL.rows, imageL.cols*2, CV_8UC1);
  cv::hconcat(imageL, imageR, stereo_pair);
  cv::ximgproc::getDisparityVis(disp,dispvis,3);

  //cv::imshow("solved disparity experimental", Resize(dispvis));
  //cv::imshow("Stereo Experimental", stereo_pair);
  //cv::waitKey(2000);
  //std::cout << "Disp type: " << disp.type() << std::endl;

  disp.convertTo(dispmath, CV_32F, 1.0/16.0, 0.0);
  //cv::multiply(dispmath,4,dispmath);
  dispmath = dispmath/4;

  // Compute average depth
  int rows = dispmath.rows;
  int cols = dispmath.cols;

  // Calculate the center of the disparity map
  int center_x = cols / 2;
  int center_y = rows / 2;
  int circle_radius = 376;
  // Calculate the average disparity within the circle
  double total_disp = 0.0;
  int num_points = 0;
  for (int i = -circle_radius; i <= circle_radius; i++) {
      for (int j = -circle_radius; j <= circle_radius; j++) {
          int x = center_x + j;
          int y = center_y + i;
          if (x >= 0 && x < cols && y >= 0 && y < rows) {
              double disp = dispmath.at<float>(y, x);
              if (disp > 0) {
                  total_disp += disp;
                  num_points++;
              }
          }
      }
  }

  // Calculate the average depth within the circle
  if (num_points > 0) {
      double avg_disp = total_disp / num_points;
      //double avg_depth = (-100 * 9.3168) / avg_disp;
      avg_depth = (9.3168*avg_disp)/-100; //8.5
      //std::cout << "Average Disparity: " << avg_disp << "|| Average Depth: " << avg_depth << std::endl;
  }

  /*MODIFIED MARCH 11, 23'
  cv::Mat imagedisparity8U = Mat( imageL.rows, imageL.cols, CV_8UC1 );
  cv::Ptr<cv::StereoMatcher> pStereo = cv::StereoBM::create(0,7);
  pStereo -> compute(imageL,imageR,imagedisparity8U);
  cv::ximgproc::getDisparityVis(imagedisparity8U,imagedisparity8U_VIS,1.0);

  Ptr<StereoMatcher> right_matcher = cv::ximgproc::createRightMatcher(pStereo);
  cv::Ptr<cv::ximgproc::DisparityWLSFilter> wls_filter = cv::ximgproc::createDisparityWLSFilter(pStereo);
  right_matcher -> compute(imageR,imageL,dispR);
  wls_filter -> setLambda(9000);
  wls_filter -> setSigmaColor(0.8);
  wls_filter -> filter(imagedisparity8U,imageL,imagedisparity8U_FILT,dispR);
  cv::ximgproc::getDisparityVis(imagedisparity8U_FILT,imagedisparity8U_FILT_VIS);

  cv::imshow("StereoBM disparity", Resize(imagedisparity8U_VIS));
  cv::imshow("StereoBM disparity WLS", Resize(imagedisparity8U_FILT_VIS));
  */

  return avg_depth;
}


cv::Mat getDisparity(cv::Mat imageL, cv::Mat imageR, Mat Q){
  //Trying new stuff here to get more accurate results
  const int minDisparity = 0;
  const int numDisparities = 80;//0 , 80
  const int blockSize = 7;//21, 7
  const int P1 = 8*blockSize*blockSize;
  const int P2 = 32*blockSize*blockSize;
  const int disp12MaxDiff = -1;
  const int preFilterCap = 0;
  const int uniquenessRatio = 55;
  const int speckleWindowSize = 10;
  const int speckleRange = 5;
  double col = imageL.cols;
  double row = imageR.rows;
  //cv::Mat_ <float> Qfloat = Q;

  cv::Ptr<cv::StereoMatcher> pStereo = cv::StereoSGBM::create	(	minDisparity,
                                                                numDisparities,
                                                                blockSize,
                                                                P1,
                                                                P2,
                                                                disp12MaxDiff,
                                                                preFilterCap,
                                                                uniquenessRatio,
                                                                speckleWindowSize,
                                                                speckleRange,
                                                                StereoSGBM::MODE_SGBM
                                                                )		;

  cv::Ptr<cv::ximgproc::DisparityWLSFilter> wls_filter = cv::ximgproc::createDisparityWLSFilter(pStereo);
  Ptr<StereoMatcher> right_matcher = cv::ximgproc::createRightMatcher(pStereo);

  cv::Mat disp,dispR,dispfilt,dispVIS,dispfiltVIS,multiout,X,Y,Z,W,dispdisplay,imageLcolor, window;
  vector<Mat>channels(3);
  //createHanningWindow(window, imageL.size(), CV_32F);
 // Point2d stereo_phase_fix=cv::phaseCorrelate(imageL,imageR,window);
  //Phase_Stitch(stereo_phase_fix,imageL,imageR);
 // cv::waitKey(0);
  imageL.convertTo(imageL,CV_8UC1,255.0);
  imageR.convertTo(imageR,CV_8UC1,255.0);
  cv::Mat mask = cv::Mat::zeros(imageL.size(),imageL.type());
  cv::Mat dstImage = cv::Mat::zeros(imageL.size(),imageL.type());

  cv::circle(mask,cv::Point(mask.cols/2,mask.rows/2),800,cv::Scalar(255,0,0),-1,8,0);
  //stereo->compute(imageR,imageL,disp);

  pStereo->compute(imageL,imageR,disp);
  right_matcher -> compute(imageR,imageL,dispR);
  wls_filter -> setLambda(10000);
  wls_filter -> setSigmaColor(0.8);
  wls_filter -> filter(disp,imageL,dispfilt,dispR);
  cv::ximgproc::getDisparityVis(disp,dispVIS,3);
  cv::ximgproc::getDisparityVis(dispfilt,dispfiltVIS);
  std::string wlsdisp = "wlsdisp";
  //cv::imshow("solved disparity", Resize(dispVIS));
  //cv::imshow(wlsdisp, Resize(dispfiltVIS));
  //cv::imshow("test 8UCI R", Resize(imageR));
  //cv::imshow("test 8UCI L", Resize(imageL));


/*
  cv::Mat_<cv::Vec3f> XYZ(disp.rows,disp.cols);   // Output point cloud
  cv::Mat_<float> vec_tmp(4,1);
  for(int y=0; y<disp.rows; ++y) {
      for(int x=0; x<disp.cols; ++x) {
          vec_tmp(0)=x; vec_tmp(1)=y; vec_tmp(2)=disp.at<float>(y,x); vec_tmp(3)=1;
          vec_tmp = Qfloat*vec_tmp;
          vec_tmp /= vec_tmp(3);
          cv::Vec3f& point = XYZ.at<cv::Vec3f>(y,x);
                  point[0] = vec_tmp(0);
                  point[1] = vec_tmp(1);
                  point[2] = vec_tmp(2);
                  //double altout = point[2];

      }
  }

//double altout = point[2];
cv::Vec3f resultZpointer = point;
std::cout << "Mean W: " << point << std::endl;


  xyz.forEach<Vec3f>(
  [](Vec3f& val, const int *pos)
  {
      if(isnan(val[0]) || if(val[0]))
          val = Vec3f();
  });
*/
  //custom depth map attempts
  double focal = 2.6997352557440677e+03;
  double focal2 = 9.3168;
  W = (focal * -100) / disp;
  cv::Scalar meanW,Wstd,meanWmask,Wstdmask;
  cv::meanStdDev(W,meanW,Wstd);
  cv::meanStdDev(W,meanWmask,Wstdmask,mask);
  std::cout << "Mean W: " << meanW << std::endl;
  std::cout << "Std W: " << Wstd << std::endl;
  std::cout << "Mean W MASK: " << meanWmask << std::endl;
  std::cout << "Std W MASK: " << Wstdmask << std::endl;
  //disp.convertTo(disp,CV_32FC1, 1.0/255.0);
  //reproject3D attempts
  cv::reprojectImageTo3D(disp,multiout, Q, true, -1);

  split(multiout,channels);
  X = channels[0];
  Y = channels[1];
  Z = channels[2];
  cv::Scalar meanZ,Zstd,meanZmask,Zstdmask;
  cv::meanStdDev(Z,meanZ,Zstd);
  //cv::meanStdDev(Z,meanZmask,Zstdmask,mask);
  cv::meanStdDev(Z,meanZmask,Zstdmask,mask);
  //cv::Scalar meanZ = cv::mean(Z);
  double meandepth = -1*sum(meanZmask)[0];
  //std::cout << "Average Depth in mm: " << meandepth << std::endl;
  //cv::imshow("Depth?: ",W);
  std::cout << "Mean Z: " << meanZ << std::endl;
  std::cout << "Std Z: " << Zstd << std::endl;
  std::cout << "Mean Z MASK: " << meanZmask << std::endl;
  std::cout << "Std Z MASK: " << Zstdmask << std::endl;


  cv::viz::Viz3d viewer;
  imageL.convertTo(imageLcolor,CV_32FC3);

  cv::viz::Color color;
  //color = cv::viz::Color::red();
  color = cv::COLORMAP_HOT;
  cv::viz::WCloud cloud(multiout,imageL);
  viewer.showWidget("Cloud Test",cloud);
  viewer.spinOnce(333,true);
  //result = 11.11; //test
  //cv::waitKey(0);

  return disp;//meandepth;

}

cv::Vec3d get3D(cv::Mat disp,cv::Mat Q){

  /*
  cv::Mat_<float> vec(4,1);

  vec(0) = col;
  vec(1) = row;
  vec(2) = this->disparity_sptr->at<float>(row,col);

  // Discard points with 0 disparity
  if(vec(2)==0) return NULL;
  vec(3)=1;
  vec = Q_32F*vec;
  vec /= vec(3);
  // Discard points that are too far from the camera, and thus are highly
  // unreliable
  if(abs(vec(0))>10 || abs(vec(1))>10 || abs(vec(2))>10) return NULL;

  cv::Vec3f *point3f = new cv::Vec3f();
  (*point3f)[0] = vec(0);
  (*point3f)[1] = vec(1);
  (*point3f)[2] = vec(2);
  */
  int y = 0;
  int x = 0;
  cv::Mat_<cv::Vec3d> XYZ(disp.rows,disp.cols);   // Output point cloud
  cv::Vec3d &point = XYZ.at<cv::Vec3d>(y,x);
  cv::Mat_<double> vec_tmp(4,1);
  for(int y=0; y<disp.rows; ++y) {
      for(int x=0; x<disp.cols; ++x) {
          vec_tmp(0)=x; vec_tmp(1)=y; vec_tmp(2)=disp.at<float>(y,x); vec_tmp(3)=1;
          vec_tmp = Q*vec_tmp;
          vec_tmp /= vec_tmp(3);
          cv::Vec3d &point = XYZ.at<cv::Vec3d>(y,x);
          point[0] = vec_tmp(0);
          point[1] = vec_tmp(1);
          point[2] = vec_tmp(2);
      }
  }

  return point;
}


Matrix<double,3,1> Translate(Point2d phase_fix, double depth, cv::Mat A){
  //Matrix<double, 3, 3>K; //camera matrix
  Matrix3d K;
  cv2eigen(A,K);

  //std::cout << "K matrix check " << K << std::endl;

  Matrix<double, 3, 1>M;
  M << (4*phase_fix.x) + 2.0583815031249073e+03,//2056 * 4 // 1028
       (4*phase_fix.y) + 1.4559202790089334e+03,//1504 * 4 in phase_fix. //752
       1.0;


  Matrix<double, 3, 1>R;

  K = K.inverse().eval();
  double img_x = ((phase_fix.x * 4) * -depth )/ 2700;
  double img_y = ((phase_fix.y * 4) * -depth )/ 2700;

  R = depth * K * M;
  //std::cout <<"Translation in mm: "<< std::endl << R << std::endl;
  //std::cout <<"Translation X TEST: "<< std::endl << img_x << std::endl;
  //std::cout <<"Translation Y TEST: "<< std::endl << img_y << std::endl;
  //result.x = R(1,1);
  //result.y = R(2,1);
  return R;
}


void TemplateClass::spin(){
  //ros::Rate loop_rate(100); // in hz
  ros::spin();
//  while (ros::ok())
//  {
//    spinOnce();
//    //loop_rate.sleep();
//  }
}



